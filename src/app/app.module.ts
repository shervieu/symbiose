import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HttpClientModule } from '@angular/common/http';
import { MatButtonModule, MatDialogModule, MatDividerModule, MatIconModule, MatTooltipModule } from '@angular/material';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { GameOverDialogComponent } from './dialogs/game-over-dialog.component';
import { MapComponent } from './layout/map/map.component';
import { MenuComponent } from './layout/menu/menu.component';
import { TeamComponent } from './layout/menu/team/team.component';
import { MenuBarComponent } from './layout/menu-bar/menu-bar.component';

@NgModule({
    declarations: [
        AppComponent,
        MenuComponent,
        MapComponent,
        GameOverDialogComponent,
        TeamComponent,
        MenuBarComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        MatIconModule,
        MatDialogModule,
        FlexLayoutModule,
        MatButtonModule,
        HttpClientModule,
        MatDividerModule,
        MatTooltipModule
    ],
    providers: [],
    entryComponents: [
        GameOverDialogComponent
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
