import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { environment } from 'src/environments/environment';
import { MatIconRegistry } from '@angular/material';
import { GameEngineService } from './game-engine/game-engine.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
    title = 'Symbiose';
    public version = environment.version;

    constructor(
        private matIconRegistry: MatIconRegistry,
        private domSanitizer: DomSanitizer,
        private gameEngine: GameEngineService
    ) {
        this.matIconRegistry.addSvgIcon(
            `knockout`,
            this.domSanitizer.bypassSecurityTrustResourceUrl(`../assets/icons/knockout.svg`)
        );
        this.matIconRegistry.addSvgIcon(
            `knockout-white`,
            this.domSanitizer.bypassSecurityTrustResourceUrl(`../assets/icons/knockout.svg`)
        );
        this.matIconRegistry.addSvgIcon(
            `contract`,
            this.domSanitizer.bypassSecurityTrustResourceUrl(`../assets/icons/contract.svg`)
        );
        this.matIconRegistry.addSvgIcon(
            `person`,
            this.domSanitizer.bypassSecurityTrustResourceUrl(`../assets/icons/person.svg`)
        );
        this.matIconRegistry.addSvgIcon(
            `campfire`,
            this.domSanitizer.bypassSecurityTrustResourceUrl(`../assets/icons/campfire.svg`)
        );
        this.matIconRegistry.addSvgIcon(
            `pawprint`,
            this.domSanitizer.bypassSecurityTrustResourceUrl(`../assets/icons/pawprint.svg`)
        );
        this.matIconRegistry.addSvgIcon(
            `pause-button`,
            this.domSanitizer.bypassSecurityTrustResourceUrl(`../assets/icons/pause-button.svg`)
        );
        this.matIconRegistry.addSvgIcon(
            `save-w`,
            this.domSanitizer.bypassSecurityTrustResourceUrl(`../assets/icons/save-w.svg`)
        );
        this.matIconRegistry.addSvgIcon(
            `load-w`,
            this.domSanitizer.bypassSecurityTrustResourceUrl(`../assets/icons/load-w.svg`)
        );
        this.matIconRegistry.addSvgIcon(
            `trash-can-w`,
            this.domSanitizer.bypassSecurityTrustResourceUrl(`../assets/icons/trash-can-w.svg`)
        );
    }

    ngOnInit() {
        this.gameEngine.boot();
    }
}
