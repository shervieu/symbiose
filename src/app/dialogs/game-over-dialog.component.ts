import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';

@Component({
    templateUrl: 'game-over-dialog.component.html',
})
export class GameOverDialogComponent {

    constructor(@Inject(MAT_DIALOG_DATA) public data: any) { }
}
