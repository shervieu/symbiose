
export enum StatusEnum {
    'RECUPERATING' = 'RECUPERATING',
    'FIGHTING' = 'FIGHTING',
    'HUNTING' = 'HUNTING',
    'WAITING' = 'WAITING',
    'FAINTED' = 'FAINTED'
}
