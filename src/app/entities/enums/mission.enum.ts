
export enum MissionEnum {
    'WAIT' = 'WAIT',
    'HUNT' = 'HUNT',
    'RECUPERATE' = 'RECUPERATE'
}
