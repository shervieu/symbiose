import { getRandomArrayElement } from '../game-engine/utils';
import { Character } from './character';

export abstract class Group {

    private _members: Character[] = [];

    constructor() { }

    public abstract isAllied(): boolean;

    public addMember(character: Character): void {
        this._members.push(character);
    }

    public get members(): Character[] {
        return this._members;
    }

    public isAlive(): boolean {
        return this.members.some((character) => character.isAlive());
    }

    public getTarget(): Character {
        return getRandomArrayElement(this.members.filter((character) => character.isAlive()));
    }
}
