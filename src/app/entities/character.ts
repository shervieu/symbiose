import { generateUUID, moreOrLessPercents } from '../game-engine/utils';
import { Group } from './group';

export interface CharacterSaveObject {
    uuid: string;
    name: string;
    hp: number;
    hpMax: number;
    strength: number;
    agility: number;
}

export class Character {

    public defense = 5;

    public static LOAD(characterSaveObject: CharacterSaveObject): Character {
        return new Character(
            characterSaveObject.name,
            characterSaveObject.hp,
            characterSaveObject.hpMax,
            characterSaveObject.strength,
            characterSaveObject.agility,
            characterSaveObject.uuid
        );
    }

    public static SAVE(character: Character): CharacterSaveObject {
        return character.SAVE();
    }

    public SAVE(): CharacterSaveObject {
        return {
            uuid: this.uuid,
            name: this.name,
            hp: this.hp,
            hpMax: this.hpMax,
            strength: this.strength,
            agility: this.agility
        };
    }

    constructor(
        public name: string,
        private _hp: number,
        public hpMax: number,
        public strength: number,
        public agility: number,
        public readonly uuid: string = generateUUID()
    ) { }

    public get hp(): number {
        return this._hp;
    }

    public set hp(hp: number) {
        this._hp = hp > 0 ? hp : 0;
    }

    public isAlive(): boolean {
        return this._hp > 0;
    }

    public getNextAction(): { nextAction: number, uuid: string } {
        return {
            nextAction: moreOrLessPercents(20000 / this.agility, 10),
            uuid: this.uuid
        };
    }

    public fight(allies: Group, ennemies: Group): void {
        this.attack(ennemies.getTarget());
    }

    private attack(target: Character) {
        const damages = Math.round(this.strength * (this.strength / target.defense));
        target.hp -= damages;
    }
}
