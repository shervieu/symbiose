import { CellDatabase } from '../game-engine/database/cell.database';
import { missionMapping } from '../game-engine/mission/mission-mapping';
import { statusMapping } from '../game-engine/mission/status/status-mapping';
import { Cell } from './cell';
import { Character, CharacterSaveObject } from './character';
import { Group } from './group';
import { MissionEnum } from './enums/mission.enum';
import { StatusEnum } from './enums/status.enum';

export interface TeamSaveObject {
    name: string;
    cell: string;
    members: CharacterSaveObject[];
    mission: MissionEnum;
    status: StatusEnum;
    statusData: any;
}

export class Team extends Group {

    public width = 50;
    public height = 150;

    public static LOAD(teamSaveObject: TeamSaveObject): Team {
        const team = new Team(
            teamSaveObject.name,
            CellDatabase[teamSaveObject.cell],
            teamSaveObject.mission,
            teamSaveObject.status,
            statusMapping[teamSaveObject.status].LOAD(teamSaveObject.statusData)
        );
        teamSaveObject.members.map(Character.LOAD).forEach((character) => team.addMember(character));
        return team;
    }

    public static SAVE(team: Team): TeamSaveObject {
        return team.SAVE();
    }

    public SAVE(): TeamSaveObject {
        return {
            name: this.name,
            cell: this.position.id,
            members: this.members.map(Character.SAVE),
            mission: this._mission,
            status: this._status,
            statusData: statusMapping[this._status].SAVE(this.statusData)
        };
    }

    constructor(
        public name: string,
        public position: Cell,
        private _mission: MissionEnum = MissionEnum.WAIT,
        private _status: StatusEnum = StatusEnum.WAITING,
        public statusData: any = {}
    ) {
        super();
    }

    public get status(): StatusEnum {
        return this._status;
    }

    public set status(status: StatusEnum) {
        statusMapping[status].initStatus(this);
        this._status = status;
    }

    public get mission(): MissionEnum {
        return this._mission;
    }

    public set mission(mission: MissionEnum) {
        if ([
            StatusEnum.WAITING,
            StatusEnum.HUNTING,
            StatusEnum.RECUPERATING,
            StatusEnum.FAINTED
        ].includes(this.status)) {
            missionMapping[mission].initMission(this);
        }
        this._mission = mission;
    }

    public isAllied(): boolean {
        return true;
    }

    public getPosX(): number {
        return this.position.getPosX() - this.getWidth() / 2;
    }

    public getPosY(): number {
        return this.position.getPosY() - this.getHeight();
    }

    public getWidth(): number {
        return this.width;
    }

    public getHeight(): number {
        return this.height;
    }
}
