export class Area {

    constructor(
        public name: string,
        public color: string
    ) { }

    toString(): string {
        return this.name;
    }
}
