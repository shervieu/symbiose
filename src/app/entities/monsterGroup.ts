import { Character, CharacterSaveObject } from './character';
import { Group } from './group';

export interface MonsterSaveObject {
    members: CharacterSaveObject[];
}

export class MonsterGroup extends Group {

    public static LOAD(monsterGroupSaveObject: MonsterSaveObject): MonsterGroup {
        const monsterGroup = new MonsterGroup();
        monsterGroupSaveObject.members.map(Character.LOAD).forEach((character) => monsterGroup.addMember(character));
        return monsterGroup;
    }

    public static SAVE(monsterGroup: MonsterGroup): MonsterSaveObject {
        return monsterGroup.SAVE();
    }

    public SAVE(): MonsterSaveObject {
        return {
            members: this.members.map(Character.SAVE),
        };
    }

    constructor() {
        super();
    }

    public isAllied(): boolean {
        return false;
    }

}
