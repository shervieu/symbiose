import { Area } from './area';

export class Cell {

    private static cellsMap: Map<string, Cell> = new Map();

    private static costier = Math.cos(Math.PI / 3);
    private static sintier = Math.sin(Math.PI / 3);
    private static cossixieme = Math.cos(Math.PI / 6);
    private static sinsixieme = Math.sin(Math.PI / 6);
    private static size = 50;
    private static posXcoef = Cell.costier * Cell.cossixieme * 2 * Cell.size;
    private static posYcoef = Cell.sintier * Cell.cossixieme * 2 * Cell.size;

    constructor(
        public x: number,
        public y: number,
        public ratio: number,
        public area: Area
    ) {
        if (x != null && y != null) {
            Cell.cellsMap.set(this.id, this);
        }
    }

    get id(): string {
        return `C_${this.x}_${this.y}`;
    }

    public getPosX(): number {
        return (this.x + this.y) * Cell.posXcoef;
    }

    public getPosY(): number {
        return (this.x - this.y) * Cell.posYcoef;
    }

    public getPoints(): string {
        const x = this.getPosX();
        const y = this.getPosY();
        const pts = (x) + ',' + (y - Cell.size) + ' '
            + (x - Cell.size * Cell.cossixieme) + ',' + (y - Cell.size * Cell.sinsixieme) + ' '
            + (x - Cell.size * Cell.cossixieme) + ',' + (y + Cell.size * Cell.sinsixieme) + ' '
            + (x) + ',' + (y + Cell.size) + ' '
            + (x + Cell.size * Cell.cossixieme) + ',' + (y + Cell.size * Cell.sinsixieme) + ' '
            + (x + Cell.size * Cell.cossixieme) + ',' + (y - Cell.size * Cell.sinsixieme);
        return pts;
    }

    public getNextCells(): Cell[] {
        const result = [];
        [
            (this.x) + ',' + (this.y + 1),
            (this.x) + ',' + (this.y - 1),
            (this.x - 1) + ',' + (this.y),
            (this.x + 1) + ',' + (this.y),
            (this.x - 1) + ',' + (this.y - 1),
            (this.x + 1) + ',' + (this.y + 1)
        ].forEach((position) => {
            if (Cell.cellsMap.has(position)) {
                result.push(Cell.cellsMap.get(position));
            }
        });
        this.shuffle(result);
        return result;
    }

    private shuffle(a) {
        for (let i = a.length - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            [a[i], a[j]] = [a[j], a[i]];
        }
        return a;
    }
}
