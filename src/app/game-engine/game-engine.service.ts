import { environment } from 'src/environments/environment';
import { Injectable, OnInit } from '@angular/core';
import { Area } from '../entities/area';
import { Cell } from '../entities/cell';
import { Character } from '../entities/character';
import { MissionEnum } from '../entities/enums/mission.enum';
import { Team, TeamSaveObject } from '../entities/team';
import { CellDatabase } from './database/cell.database';
import { missionMapping } from './mission/mission-mapping';

@Injectable({
    providedIn: 'root'
})
export class GameEngineService {

    private TIC_RATE = 25;

    private _cells: Cell[] = [];
    private _teams: Team[] = [];

    private missionMapping = missionMapping;

    constructor() { }

    boot() {
        this.initDatabase();
        const version = localStorage.getItem('version');
        if (!version) {
            localStorage.setItem('version', environment.version);
            return this.initGameData();
        }
        this.dataLoading();
    }

    get cells(): Cell[] {
        return this._cells;
    }

    get teams(): Team[] {
        return this._teams;
    }

    public dataSaving(): void {
        const save = this._teams.map(Team.SAVE);
        const encoded_save = btoa(JSON.stringify(save));
        localStorage.setItem('save', encoded_save);
    }

    public dataLoading(): void {
        while (this._teams.length > 0) {
            this._teams.pop();
        }
        const encoded_save = localStorage.getItem('save');
        const save: TeamSaveObject[] = JSON.parse(atob(encoded_save));
        save.map(Team.LOAD).forEach((team) => this.addTeamToLifeCycle(team));
    }

    private async addTeamToLifeCycle(team: Team): Promise<void> {
        this._teams.push(team);
        let timestamp = new Date().getTime();
        setInterval(() => {
            const newTimestamp = new Date().getTime();
            let diff = newTimestamp - timestamp;
            timestamp = newTimestamp;
            while (diff > 0) {
                diff = this.missionMapping[team.mission].run(team, diff);
            }
        }, 1000 / this.TIC_RATE);
    }

    public initGameData(): void {
        while (this._teams.length > 0) {
            this._teams.pop();
        }
        const team1 = new Team('Team 1', CellDatabase.C_0_0);
        team1.mission = MissionEnum.HUNT;
        team1.addMember(new Character('Cloud', 10, 10, 5, 5));
        // team1.addMember(new Character('Tifa', 10, 10, 5, 5));
        this.addTeamToLifeCycle(team1);
        this.dataSaving();
    }

    private initDatabase() {
        this._cells.push(...Object.values(CellDatabase));
    }

}
