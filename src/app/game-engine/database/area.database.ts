import { Area } from 'src/app/entities/area';

const AreaDatabase: { [key: string]: Area } = {
    TEST_AREA: new Area('Test', 'white')
};

export { AreaDatabase };
