import { Cell } from 'src/app/entities/cell';
import { AreaDatabase } from './area.database';

const CellDatabase: { [key: string]: Cell } = {
    C_0_0: new Cell(0, 0, 1, AreaDatabase.TEST_AREA)
};

export { CellDatabase };
