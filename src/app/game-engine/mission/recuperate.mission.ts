import { StatusEnum } from 'src/app/entities/enums/status.enum';
import { Team } from 'src/app/entities/team';
import { Mission } from './mission';
import { RecuperatingStatus } from './status/recuperating.status';
import { statusMapping } from './status/status-mapping';

export class RecuperateMission extends Mission {

    public initMission(team: Team) {
        team.status = StatusEnum.RECUPERATING;
    }

    public run(team: Team, time: number): number {
        super.initIfWaiting(team);
        return statusMapping[team.status].run(team, time);
    }
}
