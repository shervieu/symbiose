import { HuntMission } from './hunt.mission';
import { RecuperateMission } from './recuperate.mission';
import { WaitMission } from './wait.mission';

const missionMapping = {
    WAIT: new WaitMission(),
    HUNT: new HuntMission(),
    RECUPERATE: new RecuperateMission()
};

export { missionMapping };
