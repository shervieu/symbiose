import { StatusEnum } from 'src/app/entities/enums/status.enum';
import { Team } from 'src/app/entities/team';
import { Mission } from './mission';
import { statusMapping } from './status/status-mapping';

export class WaitMission extends Mission {

    public initMission(team: Team) {
        team.status = StatusEnum.WAITING;
    }

    public run(team: Team, time: number): number {
        return statusMapping[team.status].run(team, time);
    }
}
