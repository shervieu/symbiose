import * as _ from 'lodash';
import { Character } from 'src/app/entities/character';
import { MissionEnum } from 'src/app/entities/enums/mission.enum';
import { MonsterGroup, MonsterSaveObject } from 'src/app/entities/monsterGroup';
import { StatusEnum } from 'src/app/entities/enums/status.enum';
import { Team } from 'src/app/entities/team';
import { Status } from './status';

interface FightingStatusDataObject {
    monsterGroup: MonsterGroup;
    turns: Array<{ nextAction: number, uuid: string }>;
}

interface FightingStatusSaveObject {
    monsterGroup: MonsterSaveObject;
    turns: Array<{ nextAction: number, uuid: string }>;
}

export class FightingStatus extends Status {

    public SAVE(statusData: FightingStatusDataObject): FightingStatusSaveObject {
        return {
            monsterGroup: MonsterGroup.SAVE(statusData.monsterGroup),
            turns: statusData.turns
        };
    }

    public LOAD(statusData: FightingStatusSaveObject): FightingStatusDataObject {
        return {
            monsterGroup: MonsterGroup.LOAD(statusData.monsterGroup),
            turns: statusData.turns
        };
    }

    public initStatus(team: Team) {
        const monsterGroup = new MonsterGroup();
        const monster1 = new Character('Skely', 5, 5, 4, 5);
        monsterGroup.addMember(monster1);
        const statusData: FightingStatusDataObject = {
            monsterGroup,
            turns: _.concat(
                team.members.filter((character) => character.isAlive()),
                monsterGroup.members.filter((character) => character.isAlive())
            ).map((character) => character.getNextAction())
        };
        team.statusData = statusData;
    }

    public run(team: Team, time: number): number {
        while (time > 0 && team.isAlive() && team.statusData.monsterGroup.isAlive()) {
            team.statusData.turns.sort((a, b) => (a.nextAction - b.nextAction));
            if (team.statusData.turns[0].nextAction >= time) {
                team.statusData.turns.forEach((turn) => turn.nextAction -= time);
                time = 0;
                continue;
            }
            const diff = team.statusData.turns[0].nextAction;
            const currentCharacter = this.getByUUID(team, team.statusData.turns[0].uuid);
            team.statusData.turns.forEach((turn) => turn.nextAction -= diff);
            time -= diff;
            const isAllied = team.members.some((character) => character.uuid === currentCharacter.uuid);
            currentCharacter.fight(
                isAllied ? team : team.statusData.monsterGroup,
                !isAllied ? team : team.statusData.monsterGroup
            );
            team.statusData.turns = team.statusData.turns.filter((turn) => this.getByUUID(team, turn.uuid).isAlive());
            team.statusData.turns[0] = currentCharacter.getNextAction();
        }
        if (time > 0) {
            if (team.isAlive()) {
                team.status = StatusEnum.WAITING;
                return time;
            }
            team.mission = MissionEnum.WAIT;
            team.status = StatusEnum.FAINTED;
        }
        return 0;
    }

    private getByUUID(team: Team, uuid: string): Character {
        return team.members.find((character) => character.uuid === uuid)
            || team.statusData.monsterGroup.members.find((character) => character.uuid === uuid);
    }

}
