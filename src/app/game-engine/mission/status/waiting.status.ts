import { StatusEnum } from 'src/app/entities/enums/status.enum';
import { Team } from 'src/app/entities/team';
import { Status } from './status';

export class WaitingStatus extends Status {

    public initStatus(team: Team) {
        team.statusData = {};
    }

    public run(): number {
        return 0;
    }

}
