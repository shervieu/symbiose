import { StatusEnum } from 'src/app/entities/enums/status.enum';
import { FightingStatus } from './fighting.status';
import { HuntingStatus } from './hunting.status';
import { RecuperatingStatus } from './recuperating.status';
import { WaitingStatus } from './waiting.status';

const statusMapping = {
    HUNTING: new HuntingStatus(),
    FIGHTING: new FightingStatus(),
    FAINTED: new WaitingStatus(),
    WAITING: new WaitingStatus(),
    RECUPERATING: new RecuperatingStatus()
};

export { statusMapping };
