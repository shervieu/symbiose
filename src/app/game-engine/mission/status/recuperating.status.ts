import { MissionEnum } from 'src/app/entities/enums/mission.enum';
import { StatusEnum } from 'src/app/entities/enums/status.enum';
import { Team } from 'src/app/entities/team';
import { moreOrLessPercents } from '../../utils';
import { Status } from './status';

export class RecuperatingStatus extends Status {

    private static getNextTic(): number {
        return moreOrLessPercents(1000, 50);
    }

    public initStatus(team: Team) {
        team.statusData = { nextTic: RecuperatingStatus.getNextTic() };
    }

    public run(team: Team, time: number): number {
        while (time > 0 && team.members.some((character) => character.hp < character.hpMax)) {
            if (team.statusData.nextTic >= time) {
                team.statusData.nextTic -= time;
                return 0;
            }
            time -= team.statusData.nextTic;
            team.members.forEach((character) => character.hp++);
            team.statusData = { nextTic: RecuperatingStatus.getNextTic() };
        }
        if (time > 0 && team.mission === MissionEnum.RECUPERATE) {
            return 0;
        }
        team.status = StatusEnum.WAITING;
        return time;
    }

}
