import { StatusEnum } from 'src/app/entities/enums/status.enum';
import { Team } from 'src/app/entities/team';
import { moreOrLessPercents } from '../../utils';
import { Status } from './status';

export class HuntingStatus extends Status {

    private static getNextEncounter(): number {
        return moreOrLessPercents(10000, 50);
    }

    public initStatus(team: Team) {
        team.statusData = { nextEncounter: HuntingStatus.getNextEncounter() };
    }

    public run(team: Team, time: number): number {
        if (team.statusData.nextEncounter >= time) {
            team.statusData.nextEncounter -= time;
            return 0;
        }
        time -= team.statusData.nextEncounter;
        team.status = StatusEnum.FIGHTING;
        return time;
    }

}
