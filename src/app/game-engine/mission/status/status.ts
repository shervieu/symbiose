import { Team } from 'src/app/entities/team';

export abstract class Status {

    public SAVE(statusData: any): any {
        return statusData;
    }

    public LOAD(statusData: any): any {
        return statusData;
    }

    public abstract initStatus(team: Team): void;

    public abstract run(team: Team, time: number): number;
}
