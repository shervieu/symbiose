import { StatusEnum } from 'src/app/entities/enums/status.enum';
import { Team } from 'src/app/entities/team';

export abstract class Mission {

    public abstract initMission(team: Team): void;

    protected initIfWaiting(team: Team) {
        if (team.status === StatusEnum.WAITING) {
            this.initMission(team);
        }
    }

    public abstract run(team: Team, time: number): number;
}
