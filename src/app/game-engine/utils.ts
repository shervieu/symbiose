function moreOrLessPercents(value: number, percent: number): number {
    return Math.round(((Math.random() * 2 * percent - percent) / 100 + 1) * value);
}

function getRandomArrayElement<T>(array: Array<T>): T {
    return array[Math.floor(Math.random() * array.length)];
}

const generateUUID = require('uuid/v4');

export {
    moreOrLessPercents,
    getRandomArrayElement,
    generateUUID
};
