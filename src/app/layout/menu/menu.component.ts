import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { GameEngineService } from 'src/app/game-engine/game-engine.service';
import { Team } from 'src/app/entities/team';

@Component({
    selector: 'menu',
    templateUrl: './menu.component.html',
    styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

    public width = 250;

    public teams: Team[];

    public isDev = !environment.production;

    constructor(
        private gameEngine: GameEngineService
    ) { }

    ngOnInit() {
        this.teams = this.gameEngine.teams;
    }

}
