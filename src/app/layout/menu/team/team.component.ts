import { Component, Input } from '@angular/core';
import { GameOverDialogComponent } from 'src/app/dialogs/game-over-dialog.component';
import { MatDialog } from '@angular/material';
import { MissionEnum } from 'src/app/entities/enums/mission.enum';
import { StatusEnum } from 'src/app/entities/enums/status.enum';
import { Team } from 'src/app/entities/team';
import { WaitingStatus } from 'src/app/game-engine/mission/status/waiting.status';

@Component({
    selector: 'team',
    templateUrl: './team.component.html',
    styleUrls: ['./team.component.scss']
})
export class TeamComponent {

    @Input() public team: Team;

    constructor(
        private dialog: MatDialog
    ) { }

    public action(mission: MissionEnum) {
        this.team.mission = mission;
    }

    public fainted(): void {
        const gameOverDialog = this.dialog.open(GameOverDialogComponent, {
            width: '250px',
            disableClose: true,
            data: { team: this.team }
        });

        gameOverDialog.afterClosed().subscribe(result => {
            this.team.members.forEach(character => {
                character.hp = 1;
            });
            this.team.mission = MissionEnum.WAIT;
        });
    }

}
