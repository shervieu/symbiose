import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { GameEngineService } from 'src/app/game-engine/game-engine.service';

@Component({
    selector: 'menu-bar',
    templateUrl: './menu-bar.component.html',
    styleUrls: ['./menu-bar.component.scss']
})
export class MenuBarComponent {

    public isDev = !environment.production;

    constructor(
        private gameEngine: GameEngineService
    ) { }

    public save(): void {
        this.gameEngine.dataSaving();
    }

    public load(): void {
        this.gameEngine.dataLoading();
    }

    public reset(): void {
        this.gameEngine.initGameData();
    }
}
