import { Component, ElementRef, HostListener, OnInit, ViewChild } from '@angular/core';
import { GameEngineService } from 'src/app/game-engine/game-engine.service';
import { Cell } from '../../entities/cell';

@Component({
    selector: 'map',
    templateUrl: './map.component.html',
    styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {

    @ViewChild('svg') svg: ElementRef;

    public cells: Cell[];

    public zoom = 1;
    public midX = 0;
    public midY = 0;
    public offX = 0;
    public offY = 0;

    private timer;
    private isDraging: any;
    private preventClick = false;
    private waitClick = false;

    constructor(
        private gameEngine: GameEngineService
    ) { }

    ngOnInit() {
        document.addEventListener('contextmenu', (e) => {
            e.preventDefault();
        }, false);
        this.cells = this.gameEngine.cells;
        this.resize();
    }

    @HostListener('window:resize', ['$event']) resize() {
        const width = this.svg.nativeElement.clientWidth || this.svg.nativeElement.getBoundingClientRect().width;
        const height = this.svg.nativeElement.clientHeight || this.svg.nativeElement.getBoundingClientRect().height;
        this.midX = width / 2 + 125;
        this.midY = height / 2;
    }

    getTransform() {
        return 'matrix(' + this.zoom + ',0,0,' + this.zoom + ',' + (this.midX + this.offX) + ',' + (this.midY + this.offY) + ')';
    }

    // ----- Molette -----
    @HostListener('mousewheel', ['$event']) onMouseWheelChrome(event: any) {
        this.mouseWheelFunc(event);
    }

    @HostListener('DOMMouseScroll', ['$event']) onMouseWheelFirefox(event: any) {
        this.mouseWheelFunc(event);
    }

    @HostListener('onmousewheel', ['$event']) onMouseWheelIE(event: any) {
        this.mouseWheelFunc(event);
    }

    mouseWheelFunc(event: any) {
        event = window.event || event; // old IE support
        const delta = Math.max(-1, Math.min(1, (event.wheelDelta || -event.detail)));
        if (delta > 0) {
            this.mouseWheelUp(event);
        } else if (delta < 0) {
            this.mouseWheelDown(event);
        }
        // for IE
        event.returnValue = false;
        // for Chrome and Firefox
        if (event.preventDefault) {
            event.preventDefault();
        }
    }

    // ----- Click and Drag de la map -----
    onMousedown(event) {
        this.isDraging = {};
        this.isDraging.x = event.x;
        this.isDraging.y = event.y;
    }

    onMouseup() {
        if (this.isDraging.moved) {
            this.timer = setTimeout(() => {
                this.preventClick = false;
            });
        }
        this.isDraging = null;
    }

    onMousemove(event) {
        if (this.isDraging) {
            if (!this.isDraging.moved) {
                this.isDraging.moved = true;
                this.preventClick = true;
            }
            this.offX += (event.x - this.isDraging.x);
            this.offY += (event.y - this.isDraging.y);
            this.isDraging.x = event.x;
            this.isDraging.y = event.y;
        }
    }

    setZoom(zoom) {
        this.zoom = zoom < 0.4 ? 0.4 : zoom > 3 ? 3 : zoom;
    }

    zoomOnCell(cell) {
    }

    zoomOnTeam(team) {
    }


    mouseWheelUp(event) {
        const zoom = this.zoom / 0.9;
        this.setZoom(zoom);
    }

    mouseWheelDown(event) {
        const zoom = this.zoom * 0.9;
        this.setZoom(zoom);
    }

    // ----- Click sur un element -----
    onClickCell(cell) {
        if (!this.preventClick) {
            this.waitClick = false;
            this.timer = setTimeout(() => {
                if (!this.waitClick) {
                    console.log('click', cell);
                }
            }, 200);
        }
    }

    onDoubleClickCell(cell) {
        if (!this.preventClick) {
            this.waitClick = true;
            clearTimeout(this.timer);
            this.zoomOnCell(cell);
        }
    }

    onRightClickCell(cell, event) {
        if (!this.preventClick) {
        }
    }

    onClickTeam(team) {
        if (!this.preventClick) {
            this.zoomOnTeam(team);
        }
    }

    // ----- Pathfinding -----
    pathfinding(depart: Cell, destination: Cell): any {
        class Itineraire {
            constructor(public path: Cell[]) {
            }

            public score(): number {
                let score = 0;
                this.path.forEach((cell) => score += cell.ratio);
                return score;
            }

            public getNextCells() {
                return this.path[this.path.length - 1].getNextCells();
            }
        }

        const visiteds = new Map<string, Itineraire>();
        const start = new Itineraire([depart]);
        visiteds.set(depart.id, start);
        let itinerairesToTry = [start];
        while (itinerairesToTry.length > 0) {
            const nextItinerairesToTry: Itineraire[] = [];
            itinerairesToTry.forEach((itineraire) => {
                itineraire.getNextCells().forEach((cell) => {
                    if (!visiteds.has(cell.id) || visiteds.get(cell.id).score() > itineraire.score() + cell.ratio) {
                        const path = itineraire.path.map((cellOfPath) => {
                            return cellOfPath;
                        });
                        path.push(cell);
                        const newItineraire = new Itineraire(path);
                        visiteds.set(cell.id, newItineraire);
                        nextItinerairesToTry.push(newItineraire);
                    }
                });
            });
            itinerairesToTry = nextItinerairesToTry;
            // CLEAN SI CHEMIN VALIDE
            const reference = visiteds.get(destination.id);
            if (reference) {
                // Si un chemin valide a été trouvé, inutile de tester les chemins déjà plus longs que lui
                const newItinerairesToTry = [];
                itinerairesToTry.forEach((itineraire) => {
                    if (itineraire.score() < reference.score()) {
                        newItinerairesToTry.push(itineraire);
                    }
                });
                itinerairesToTry = newItinerairesToTry;
            }
        }
        const result = visiteds.get(destination.id);
        return result ? result.path : null;
    }

}
